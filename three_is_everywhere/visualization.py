import pygame, sys

def sys_init(size):
    pygame.init()
    screen = pygame.display.set_mode(size)
    pygame.display.set_caption('Three is everywhere')
    return screen

def graph_visualisation(screen):
    COLORS = {"white": (255, 255, 255), "black": (0, 0, 0), "red": (255, 0, 0), "green": (0, 255, 0), "blue": (0, 0, 255)}
    quit = False
    
    while not quit:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        screen.fill(COLORS["black"])

        show_graph(screen)

        pygame.display.flip()
        pygame.display.update()

def show_graph(screen):
    graph = pygame.image.load('fix_size.png')
    # graph_rect = graph.get_rect(50, 50, width = 500, height = 200)
    screen.blit(graph, (20, 20, 500, 100))

width, height = 900, 900
graph_visualisation(sys_init((width, height)))
